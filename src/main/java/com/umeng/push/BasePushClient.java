package com.umeng.push;

import com.alibaba.fastjson.JSONObject;
import com.umeng.push.android.AndroidBroadcast;
import com.umeng.push.android.AndroidUnicast;
import com.umeng.push.helper.HttpHelper;
import com.umeng.push.ios.IOSBroadcast;
import com.umeng.push.ios.IOSUnicast;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;


public abstract class BasePushClient {

    ;

    // The host
    private String host = "http://msg.umeng.com";

    // The post path
    private String postPath = "/api/send";

    private String appkey = "5aaf77b8f29d9844bf0002c0";

    private String appMasterSecret = "95dendldq4r4a5bf4jva2l1parhlesti";

    /**
     * 获取Api主域名
     *
     * @return
     */
    public String getHost() {
        return host;
    }

    /**
     * 路径
     *
     * @return
     */
    public String getPostPath() {
        return postPath;
    }

    /**
     * appkey
     *
     * @return
     */
    public String getAppkey() {
        return appkey;
    }

    /**
     * @return
     */
    public String getAppMasterSecret() {
        return appMasterSecret;
    }

    public boolean send(BaseUmengNotification msg) throws Exception {

        String timestamp = Integer.toString((int) (System.currentTimeMillis() / 1000));

        msg.setPredefinedKeyValue("timestamp", timestamp);

        String url = getHost() + getPostPath();

        String postBody = msg.getPostBody();

        String sign = DigestUtils.md5Hex(("POST" + url + postBody + msg.getAppMasterSecret()).getBytes(StandardCharsets.UTF_8));

        url = url + "?sign=" + sign;

        HttpHelper.getInstance().post(url, postBody);

        return true;

    }


    /**
     * 发送Android消息
     *
     * @param deviceToken 机器
     * @param title       通知标题
     * @param text        通知内容
     * @param message 自定义消息的内容
     * @return
     * @throws IOException
     */
    public boolean sendAnroid(String deviceToken, String title, String text, String message) {

        AndroidUnicast unicast = null;
        try {
            unicast = new AndroidUnicast(getAppkey(), getAppMasterSecret());

            // TODO Set your device token
            unicast.setDeviceToken(deviceToken);
            unicast.setTicker(title);
            unicast.setTitle(title);
            unicast.setText(text);
            unicast.goAppAfterOpen();
            unicast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
            // TODO Set 'production_mode' to 'false' if it's a test device.
            // For how to register a test device, please see the developer doc.
            unicast.setProductionMode();
            // Set customized fields
            unicast.setExtraField("message", message);

            System.out.println("unicast:" + JSONObject.toJSONString(unicast));

            //logs.getiLogStrategy().Write(JSON.toJSONString(unicast));

            return send(unicast);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    /**
     * 发送Android广播
     *
     * @param title 通知标题
     * @param text  通知内容
     * @return
     * @throws IOException
     */
    public boolean sendAnroid(String title, String text, String message) {
        AndroidBroadcast broadcast = null;
        try {
            broadcast = new AndroidBroadcast(getAppkey(), getAppMasterSecret());
            broadcast.setTicker(title);
            broadcast.setTitle(text);
            broadcast.setText(text);
            broadcast.goAppAfterOpen();
            broadcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
            // TODO Set 'production_mode' to 'false' if it's a test device.
            // For how to register a test device, please see the developer doc.
            broadcast.setProductionMode();
            // Set customized fields
            broadcast.setExtraField("message", message);
            send(broadcast);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    /**
     * 发送Android消息
     *
     * @param deviceToken 机器
     * @param title       通知标题
     * @param text        通知内容
     * @param message 自定义消息的内容
     * @return
     * @throws IOException
     */
    public boolean sendIOS(String deviceToken, String title, String text, String message) {

        IOSUnicast unicast = null;
        try {
            unicast = new IOSUnicast(appkey, appMasterSecret);
            // TODO Set your device token
            unicast.setDeviceToken(deviceToken);
            unicast.setAlert(title);
            unicast.setBadge(0);
            unicast.setSound("default");
            // TODO set 'production_mode' to 'true' if your app is under production mode
            unicast.setTestMode();
            // Set customized fields
            unicast.setCustomizedField(text, message);
            send(unicast);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 发送IOS广播
     *
     * @param title 通知标题
     * @param text  通知内容
     * @return
     * @throws IOException
     */
    public boolean sendIOS(String title, String text, String message) {

        try {
            IOSBroadcast broadcast = new IOSBroadcast(appkey, appMasterSecret);
            broadcast.setAlert(title);
            broadcast.setBadge(0);
            broadcast.setSound("default");
            // TODO set 'production_mode' to 'true' if your app is under production mode
            // broadcast.setTestMode();
            broadcast.setProductionMode(true);
            // Set customized fields
            broadcast.setCustomizedField(text, message);
            send(broadcast);

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }


}
