package com.umeng.push.helper;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.ByteArrayRequestEntity;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class HttpHelper {

    private static HttpHelper httpHelper = null;

    public synchronized static HttpHelper getInstance() {

        if (httpHelper == null) {
            httpHelper = new HttpHelper();
        }

        return httpHelper;
    }

    private HttpHelper() {

    }

    public String post(String url, String body) throws IOException {


        HttpClient client = null;

        try {

            client = new HttpClient();
            client.getHttpConnectionManager().getParams().setConnectionTimeout(2000);//设置连接超时时间为2秒（连接初始化时间）

            PostMethod method = new PostMethod(url);

            method.setRequestHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
            method.setRequestHeader("Accept-Charset", "utf-8,GB2312;q=0.7,*;q=0.7");
            method.setRequestHeader("Accept-Language", "zh-cn,zh;q=0.5");
            method.setRequestHeader("Connection", "Keep-Alive");
            method.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");

            RequestEntity requestEntity = new ByteArrayRequestEntity(body.getBytes(StandardCharsets.UTF_8));

            method.setRequestEntity(requestEntity);

            int statusCode = client.executeMethod(method);

            if (statusCode == 200) {
                System.out.println("Notification sent successfully.");
            } else {
                System.out.println("Failed to send the notification!");
            }

            return method.getResponseBodyAsString();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (client != null) {
                client.getHttpConnectionManager().closeIdleConnections(1);
            }
        }

        throw new IOException("网络异常");
    }


    public String get(String url) throws IOException {


        HttpClient client = null;

        try {

            client = new HttpClient();
            client.getHttpConnectionManager().getParams().setConnectionTimeout(2000);//设置连接超时时间为2秒（连接初始化时间）

            GetMethod method = new GetMethod(url);

            method.setRequestHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
            method.setRequestHeader("Accept-Charset", "utf-8,GB2312;q=0.7,*;q=0.7");
            method.setRequestHeader("Accept-Language", "zh-cn,zh;q=0.5");
            method.setRequestHeader("Connection", "Keep-Alive");
            method.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");


            int statusCode = client.executeMethod(method);

            if (statusCode == 200) {
                System.out.println("Notification sent successfully.");
            } else {
                System.out.println("Failed to send the notification!");
            }

            return method.getResponseBodyAsString();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (client != null) {
                client.getHttpConnectionManager().closeIdleConnections(1);
            }
        }

        throw new IOException("网络异常");
    }

}
